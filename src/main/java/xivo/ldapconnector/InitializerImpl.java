package xivo.ldapconnector;

import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import xivo.ldap.Initializer;
import xivo.restapi.model.User;

public class InitializerImpl implements Initializer {

    private List<User> usersToUpdate;
    private List<User> unmatchedXivoUsers;
    private Logger logger = Logger.getLogger(getClass().getName());

    public void initialize(List<User> xivoUsers, List<User> ldapUsers) {
        usersToUpdate = new LinkedList<User>();
        unmatchedXivoUsers = new LinkedList<User>();
        for (User ldapUser : ldapUsers) {
            User xivoUser = findMatchingXivoUser(ldapUser, xivoUsers);

            if (userNeedsUpdate(xivoUser)) {
                xivoUser.setDescription(ldapUser.getDescription());
                usersToUpdate.add(xivoUser);
            }
            xivoUsers.remove(xivoUser);
        }
        unmatchedXivoUsers = xivoUsers;
        logger.info("Logging unmatched xivo users to /var/log/xivo-ldap-synchronizer/unmatched_xivo_users.log");
        File file = new File("/var/log/xivo-ldap-synchronizer/unmatched_xivo_users.log");
        try {
            FileUtils.writeLines(file, unmatchedXivoUsers);
        } catch (IOException e) {
            logger.severe("Could not write unmatched xivo users to log file");
        }
    }

    private User findMatchingXivoUser(User ldapUser, List<User> xivoUsers) {
        logger.info("######### INITIALIZATION MODE step: User LDAP to find: " + ldapUser.getFirstname() + ldapUser.getLastname());
        for (User xivoUser : xivoUsers) {
            if (stringsMatch(xivoUser.getFirstname().replaceAll("\\s",""), ldapUser.getFirstname().replaceAll("\\s",""))
                    && stringsMatch(xivoUser.getLastname().replaceAll("\\s",""), ldapUser.getLastname().replaceAll("\\s",""))) {
                logger.info(
                        "######### INITIALIZATION MODE step: Thanks to firstname & lastname & sAMAccountname, matched ldapUser:"
                                + ldapUser + " with xivoUser:" + xivoUser);
                return xivoUser;
            } 
            if ((xivoUser.getVoicemail() != null && ldapUser.getVoicemail() != null)
                    && (xivoUser.getVoicemail().getEmail() != null && ldapUser.getVoicemail().getEmail() != null)) {
                if (stringsMatch(xivoUser.getVoicemail().getEmail(), ldapUser.getVoicemail().getEmail())) {
                    logger.info("######### INITIALIZATION STEP: Thanks to email, matched ldapUser:" + ldapUser
                            + " with xivoUser:" + xivoUser);
                    return xivoUser;
                }
              
            }
          if (xivoUser.getUsername() != null && ldapUser.getDescription() != null) {
        	  if (xivoUser.getUsername().equals(ldapUser.getDescription())) {
        	  return xivoUser;
          }
        }
        logger.info("!! ==========> No matching XiVO user found for ldap user: " + ldapUser);
        
    }
        return null;
    }

    private boolean userNeedsUpdate(User xivoUser) {
        return xivoUser != null && (xivoUser.getDescription() == null || xivoUser.getDescription().equals(""));
    }

    public List<User> getUsersToUpdate() {
        return usersToUpdate;
    }

    public List<User> getUnmatchedXivoUsers() {
        return unmatchedXivoUsers;
    }

    private boolean stringsMatch(String s1, String s2) {
        s1 = normalize(s1);
        s2 = normalize(s2);
        return s1.equalsIgnoreCase(s2);
    }

    private String normalize(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        return s.replaceAll("[^\\p{ASCII}]", "");
    }
}
