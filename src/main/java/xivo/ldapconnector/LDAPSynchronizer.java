package xivo.ldapconnector;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.naming.NamingEnumeration;
import javax.naming.directory.SearchResult;

import org.apache.commons.cli.ParseException;

import com.google.inject.Inject;

import xivo.ldap.AbstractLdapSynchronizer;
import xivo.ldap.Initializer;
import xivo.ldap.asterisk.AsteriskManager;
import xivo.ldap.ldapconnection.LDAPConnector;
import xivo.ldap.xivoconnection.XivoConnector;
import xivo.restapi.connection.WebServicesException;

public class LDAPSynchronizer extends AbstractLdapSynchronizer {

    private static Logger logger;
    
    
    public static void main(String[] args) throws IOException, ParseException {
        start(LDAPModule.class, args);
    }

    private AsteriskManager asterisk;

    @Inject
    public LDAPSynchronizer(XivoConnector xivoConnector, LDAPConnector ldapConnector, DeltaGenerator deltaGenerator,
            Initializer initializer, AsteriskManager asterisk) {
        super();
        this.xivoConnector = xivoConnector;
        this.ldapConnector = ldapConnector;
        this.deltaGenerator = deltaGenerator;
        this.initializer = initializer;
        this.asterisk = asterisk;
        logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    protected void setUp() {
        // logger.info("Nothing particular to be done");
        logger.info(
                "\n\n[** GENERAL INFO **] Xivo Needs Update criteria: checkFirstname, checkLastname, checkEmail, checkXiVOLogin, checkXiVOPassword& checkMobile \n\n");
        
    }
    
    @Override
    protected void synchronize() throws IOException, WebServicesException, SQLException {
        deltaGenerator.generateDelta(xivoUsers, ldapUsers);
        logger.info("Disabling live reload");
        xivoConnector.disableLiveReload();
        performCreations();
        performUpdates();
        performDeletions();
        performLdapUpdates();
        performSpecificActions();
        if(config.mustReenableLiveReload()) {
            logger.info("Reenabling live reload");
            xivoConnector.enableLiveReload();
        }
        asteriskManager.reloadCore();
    }
    


    @Override
    protected void performSpecificActions() {
        asterisk.reloadVoicemails();
        asterisk.reloadDialplan();
    }
    
    @Override
    protected void initialize() throws IOException, WebServicesException, SQLException {
        super.initialize();
        ldapConnector.updateUsers(initializer.getUsersToUpdate(), config.getUpdatedLdapFields());
    }
}
