package xivo.ldapconnector;

import java.util.logging.Logger;

import xivo.ldap.AbstractDeltaGenerator;
import xivo.restapi.model.User;


public class DeltaGenerator extends AbstractDeltaGenerator {

    Logger logger = Logger.getLogger(getClass().getName());
    boolean logger_boolean = false;

    @Override
    protected boolean xivoNeedsUpdate(User xivoUser, User ldapUser) {

        logger.info("[** XiVO NEEDS UPDATE **] Is Xivo user " + xivoUser.getFirstname() + " " + xivoUser.getLastname()
                + " differs from Ldap user " + ldapUser.getFirstname() + " " + ldapUser.getLastname()
                + " (based on previous criteria)?");
        logger_boolean = userNeedsUpdate(xivoUser, ldapUser);
        logger.info("[** XiVO NEEDS UPDATE **] ==> " + logger_boolean + "\n\n");
        return logger_boolean;
    }

    private boolean userNeedsUpdate(User xivoUser, User ldapUser) {
        return checkFirstname(xivoUser, ldapUser) || checkLastname(xivoUser, ldapUser) 
        		|| checkEmail(xivoUser, ldapUser) || checkMobile(xivoUser, ldapUser) || checkXiVOClientLogin(xivoUser,ldapUser) || checkXiVOClientMdp(xivoUser)
        		|| checkUserfield(xivoUser,ldapUser);
    }

    private boolean checkFirstname(User xivoUser, User ldapUser) {

        if (ldapUser.getFirstname() == null && xivoUser.getFirstname() != null) {
            return true;
        }

        if (((ldapUser.getFirstname() != null) || (xivoUser.getFirstname() != null))
                && (!ldapUser.getFirstname().equals(xivoUser.getFirstname()))) {
            logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname()+ " update his firstname to: "
                    + ldapUser.getFirstname() + "\n\n");
            return true;
        } else {
            return false;
        }
    }

    private boolean checkLastname(User xivoUser, User ldapUser) {

        if (ldapUser.getLastname() == null && xivoUser.getLastname() != null) {
            return true;
        }

        if (((ldapUser.getLastname() != null) || (xivoUser.getLastname() != null))
                && (!ldapUser.getLastname().equals(xivoUser.getLastname()))) {
            logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname() + " update his lastname to: "
                    + ldapUser.getLastname() + "\n\n");
            return true;
        } else {
            return false;
        }
    }
    private boolean checkXiVOClientLogin(User xivoUser, User ldapUser) {

        boolean checkXiVOClientLogin_boolean = false;
        checkXiVOClientLogin_boolean = ldapUser.getUsername() != null
                && (!xivoUser.getUsername().equals(ldapUser.getUsername()));
        if (checkXiVOClientLogin_boolean == true) {
            logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname() + " update his Login XiVO Client to: "
                    + ldapUser.getUsername() + "\n\n");
            return checkXiVOClientLogin_boolean;
        } else {
            return checkXiVOClientLogin_boolean;
        }
    }
    
    private boolean checkUserfield(User xivoUser, User ldapUser) {

        boolean checkUserfield_boolean = false;
        checkUserfield_boolean = ldapUser.getUserfield() != null
                && (!xivoUser.getUserfield().equals(ldapUser.getUserfield()));
        if (checkUserfield_boolean == true) {
            logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname() + " update his userfield to: "
                    + ldapUser.getUserfield() + "\n\n");
            return checkUserfield_boolean;
        } else {
            return checkUserfield_boolean;
        }
    }

    private boolean checkXiVOClientMdp(User xivoUser) {

        boolean checkXiVOClientMdp_boolean = false;
        if (xivoUser.getPassword() != null) {
            checkXiVOClientMdp_boolean = (!xivoUser.getPassword().equals("B!ds5:J6gfy8M$Ugy6JB"));
            if (checkXiVOClientMdp_boolean == true) {
                logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname() 
                        + " update his MDP XiVO Client to: Bds5J6gfy8MUgy6JB \n\n");
                return checkXiVOClientMdp_boolean;
            } else {
                return checkXiVOClientMdp_boolean;
            }

        }
        return checkXiVOClientMdp_boolean;
    }
    
    /*private boolean checkXiVOClientMdp(User xivoUser) {

        boolean checkXiVOClientMdp_boolean = false;
        if (xivoUser.getPassword() != null) {
        		if (xivoUser.getLine() != null) {
        			
		            checkXiVOClientMdp_boolean = (!xivoUser.getPassword().equals(xivoUser.getLine().getNumber()));
		            if (checkXiVOClientMdp_boolean == true) {
		                logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname()
		                        + " update his MDP XiVO Client to:  default \n\n");
		                return checkXiVOClientMdp_boolean;
		            } else {
		                return checkXiVOClientMdp_boolean = true;
		            }
        		}
        }
        return checkXiVOClientMdp_boolean;
    }
    */
    
    private boolean checkVoicemail(User xivoUser, User ldapUser) {

        boolean checkVoicemail_boolean = false;
        if (ldapUser.getVoicemail() != null && xivoUser.getVoicemail() != null) {
            checkVoicemail_boolean = ldapUser.getVoicemail() != null
                    && (xivoUser.getVoicemail() == null || xivoUser.getVoicemail().getEmail() == null
                            || !xivoUser.getVoicemail().getEmail().equals(ldapUser.getVoicemail().getEmail())
                            || !xivoUser.getVoicemail().getName().equals(ldapUser.getVoicemail().getName()));
        }
        if (checkVoicemail_boolean == true) {
            logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname() + " update his Voicemail Email to: "
                    + ldapUser.getVoicemail().getEmail() + "\n\n");
            logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname()+ " update his Voicemail Name to: "
                    + ldapUser.getVoicemail().getName() + "\n\n");
            return checkVoicemail_boolean;
        } else {
            return checkVoicemail_boolean;
        }
    }


    private boolean checkEmail(User xivoUser, User ldapUser) {

        if (ldapUser.getEmail() == null && xivoUser.getEmail() != null) {
            return true;
        }

        if (((ldapUser.getEmail() != null) || (xivoUser.getEmail() != null))
                && (!ldapUser.getEmail().equals(xivoUser.getEmail()))) {
            logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname() + " update his email to: "
                    + ldapUser.getEmail() + "\n\n");
            return true;
        } else {
            return false;
        }
    }
    
    private boolean checkMobile(User xivoUser, User ldapUser) {

        boolean checkMobile_boolean = false;
        if (xivoUser.getMobilephonenumber() != null) {
            checkMobile_boolean = ldapUser.getMobilephonenumber() != null  || !(xivoUser.getMobilephonenumber()).equals(ldapUser.getMobilephonenumber());
            logger.info("\n\n[** MOBILE NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname() + xivoUser.getMobilephonenumber());
            if (checkMobile_boolean == true) {
                logger.info("\n\n[** XiVO NEEDS UPDATE **] " + xivoUser.getFirstname() + " " + xivoUser.getLastname() + " update his Mobile number to: "
                        + ldapUser.getMobilephonenumber() + "\n\n");
                return checkMobile_boolean;
            } else {
                return checkMobile_boolean;
            }
        }
        return checkMobile_boolean;
    }



    public String formatWithSpace(String SDA) {
    	// format to XX XX XX XX XX
    	// ici les numéro sont sur 10 chiffres avaec des espcaces, donc on enlève ces espaces d'abord.
    	
		String first;
		String second;
		String third;
		String fourth;
		String fifth;
		String Number = new String();
		Number=SDA.replaceAll("\\s", "");
		first = "0"+Number.substring(0,1);
		second = Number.substring(1,3);
		third = Number.substring(3,5);
		fourth = Number.substring(5,7);
		fifth = Number.substring(7,9);
		
		Number=first+" "+second+" "+third+" "+fourth+" "+fifth;
		;;logger.info("SDA reformatée : "+Number);
		return Number;
    }
    @Override
    protected boolean ldapNeedsUpdate(User xivoUser, User ldapUser) {

    	/*if (mobileNeedsUpdate(xivoUser, ldapUser) == true) {
            logger.info("[** LDAP NEEDS UPDATE **] mobileNeedsUpdate: " + ldapUser.getFullname() + " ("
                    + ldapUser.getDescription() + ")" + " mobile phone number change to "
                    + formatWithSpace(xivoUser.getMobilephonenumber()) + "\n\n");
        }*/

        if (ProvisionningCodeNeedsUpdate(xivoUser, ldapUser) == true) {
            if (xivoUser.getLine() != null) {
                logger.info("[** LDAP NEEDS UPDATE **] ProvisionningCodeNeedsUpdate: " + ldapUser.getFirstname() + " " + ldapUser.getLastname() + " ("
                        + ldapUser.getDescription() + ")" + " provcode change to "
                        + xivoUser.getLine().getprovisioningCode() + "\n\n");
            } else {
                logger.info("[** LDAP NEEDS UPDATE **] incallNeedsUpdate: " + ldapUser.getFirstname() + " " + ldapUser.getLastname()  + " ("
                        + ldapUser.getDescription() + ")"
                        + " external LDAP number differs from XIVO but is not to update \n");
            }
        }

        if (lineNeedsUpdate(xivoUser, ldapUser) == true) {
            if (xivoUser.getLine() != null) {
                logger.info("[** LDAP NEEDS UPDATE **] lineNeedsUpdate: " + ldapUser.getFirstname() + " " + ldapUser.getLastname()  + " ("
                        + ldapUser.getDescription() + ")" + " internal number change to "
                        + xivoUser.getLine().getNumber() + "\n\n");
            } else {
                logger.info("[** LDAP NEEDS UPDATE **] lineNeedsUpdate: " + ldapUser.getFirstname() + " " + ldapUser.getLastname() + " ("
                        + ldapUser.getDescription() + ")"
                        + " internal LDAP number differs from XIVO but is not to update \n");
            }
        }

        return lineNeedsUpdate(xivoUser, ldapUser) || ProvisionningCodeNeedsUpdate(xivoUser, ldapUser);
    }

    private boolean incallNeedsUpdate(User xivoUser, User ldapUser) {
        boolean incallNeedsUpdate_boolean = false;
        if (xivoUser.getIncomingCall() != null) {
            if (ldapUser.getIncomingCall() != null) {
            	logger.info("LDAP USER IS :"+ ldapUser.getFirstname() + " " + ldapUser.getLastname());
                logger.info("LDAP INCALL IS :" + ldapUser.getIncomingCall().getSda());
                logger.info("USER INCALL IS :" + xivoUser.getIncomingCall().getSda());

                incallNeedsUpdate_boolean = (!ldapUser.getIncomingCall().getSda()
                        .equals(xivoUser.getIncomingCall().getSda()));
            } else {
            	logger.info("LDAP INCALL IS NULL , SETTING XIVO INCALL");
                incallNeedsUpdate_boolean = true;
            }
        }
        else {
        	logger.info("ici");
        }
        logger.info("LDAP USER IS :"+ ldapUser.getFirstname() + " " + ldapUser.getLastname());
        logger.info(" SDA needs UPDATE : " + incallNeedsUpdate_boolean);
        return incallNeedsUpdate_boolean;
    }
    
    private boolean ProvisionningCodeNeedsUpdate(User xivoUser, User ldapUser) {
        boolean ProvisionningCode_boolean = false;
        if (ldapUser.getLine() != null) {
        	if (ldapUser.getLine().getprovisioningCode() == null) {
        		ProvisionningCode_boolean = true;
        		return ProvisionningCode_boolean;
        	}
        	else 
        		
	        if (xivoUser.getLine() != null && ldapUser.getLine() != null) {
	            if (ldapUser.getLine() != null && ldapUser.getLine() != null) {
	            	//logger.info("LDAP USER IS :"+ ldapUser.getFirstname() + " " + ldapUser.getLastname());
	                logger.info("LDAP PROVCODE IS :" + ldapUser.getLine() + " " + ldapUser.getLine().getprovisioningCode());
	                logger.info("USER PROVCODE IS :" + xivoUser.getLine() + " " + xivoUser.getLine().getprovisioningCode());
	
	                ProvisionningCode_boolean = (!ldapUser.getLine().getprovisioningCode()
	                        .equals(xivoUser.getLine().getprovisioningCode()));
	            } else {
	            	logger.info("LDAP INCALL IS NULL , SETTING XIVO PROV CODE");
	            	ProvisionningCode_boolean = true;
	            }
	        }
	        else {
	        	logger.info("ici");
	        }
	        logger.info("LDAP USER IS :"+ ldapUser.getFirstname() + " " + ldapUser.getLastname());
	        logger.info(" PROV CODE needs UPDATE : " + ProvisionningCode_boolean);
	        return ProvisionningCode_boolean;
        }
        return ProvisionningCode_boolean;
	  }

    private boolean mobileNeedsUpdate(User xivoUser, User ldapUser) {
        boolean mobileNeedsUpdate_boolean = false;
        mobileNeedsUpdate_boolean = xivoUser.getMobilephonenumber() != null
                && !(xivoUser.getMobilephonenumber()).equals(ldapUser.getMobilephonenumber());
        logger.info(" mobile number needs UPDATE : " + mobileNeedsUpdate_boolean + " for user :" + ldapUser.getFirstname() + " " + ldapUser.getLastname() );
        return mobileNeedsUpdate_boolean;
    }

    private boolean lineNeedsUpdate(User xivoUser, User ldapUser) {
        boolean lineNeedUpdate_boolean = false;
        if (xivoUser.getLine() != null) {
            if (ldapUser.getLine() != null) {
                lineNeedUpdate_boolean = (!xivoUser.getLine().getNumber().equals(ldapUser.getLine().getNumber()));
                logger.info("LDAP USER IS :"+ ldapUser.getFirstname() + " " + ldapUser.getLastname());
                logger.info("LDAP Internal LINE IS :" + ldapUser.getLine().getNumber());
                logger.info("USER Internal LINE IS :" + xivoUser.getLine().getNumber());
            } else {
                lineNeedUpdate_boolean = true;
            }
        }

        logger.info(" internal number needs UPDATE : " + lineNeedUpdate_boolean + "  for user :" + ldapUser.getFirstname() + " " +  ldapUser.getLastname() );
        return lineNeedUpdate_boolean;
    }
    
}
