package xivo.ldapconnector;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import xivo.restapi.model.User;

public class TestInitializer {

    private List<User> ldapUsers;
    private List<User> xivoUsers;
    private User xivo1 = new User(), xivo2 = new User(), xivo3 = new User(), xivo4 = new User(), ldap1 = new User(),
            ldap2 = new User(), ldap3 = new User(), ldap4 = new User();
    private InitializerImpl initializer;

    @Before
    public void setUp() throws Exception {
        xivo1.setFirstname("JEAN");
        xivo1.setLastname("Dupond");
        xivo1.setEmail("jd@xivo.solutions");

        xivo2.setFirstname("Marc");
        xivo2.setLastname("ROCHER");
        xivo2.setEmail("mr@xivo.solutions");

        xivo3.setFirstname("Albért");
        xivo3.setLastname("Dupont");
        xivo3.setDescription("desc4");
        xivo3.setEmail("adupont@avencall.com");

        xivo4.setFirstname("jerémie");
        xivo4.setLastname("london");
        xivo4.setEmail("jlondon@avencall.com");

        ldap1.setFirstname("JEAN");
        ldap1.setLastname("Dupond");
        ldap1.setUserfield("dep 01");
        ldap1.setDescription("desc1");
        ldap1.setEmail("jdupond@avencall.com");

        ldap2.setFirstname("jerémie");
        ldap2.setLastname("london");
        ldap2.setDescription("desc3");
        ldap2.setEmail("jlondon@avencall.com");

        ldap3.setFirstname("Albert");
        ldap3.setLastname("Dupont");
        ldap3.setDescription("desc4");
        ldap3.setEmail("adupont@avencall.com");

        ldap4.setFirstname("Hubert");
        ldap4.setLastname("reeves");
        ldap4.setEmail("hreeves@avencall.com");

        xivoUsers = new LinkedList<User>(Arrays.asList(new User[] { xivo1, xivo2, xivo3, xivo4 }));
        ldapUsers = new LinkedList<User>(Arrays.asList(new User[] { ldap1, ldap2, ldap3, ldap4 }));

        initializer = new InitializerImpl();
    }

    @Test
    public void testInitialize() {
        initializer.initialize(xivoUsers, ldapUsers);

        List<User> expectedUsersToUpdate = Arrays.asList(new User[] { xivo1, xivo4 });
        List<User> expectedUnmatched = Arrays.asList(new User[] { xivo2 });
        assertEquals(expectedUsersToUpdate, initializer.getUsersToUpdate());
        assertEquals(expectedUnmatched, initializer.getUnmatchedXivoUsers());
        assertEquals("desc1", xivo1.getDescription());

    }
}
