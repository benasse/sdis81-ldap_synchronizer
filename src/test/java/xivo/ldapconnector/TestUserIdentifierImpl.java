package xivo.ldapconnector;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import xivo.ldap.ldapconnection.UserIdentifier;
import xivo.restapi.model.User;

public class TestUserIdentifierImpl {

    @Test
    public void testGetSearchCriteria() {
        UserIdentifier identifier = new UserIdentifierImpl();
        User user = new User();
        user.setDescription("s.durand");
        assertEquals("(sn=s.durand)", identifier.getSearchCriteria(user));
    }
}
