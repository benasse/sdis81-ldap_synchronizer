package xivo.ldapconnector;

import java.util.logging.Logger;

import xivo.ldap.ldapconnection.UserIdentifier;
import xivo.restapi.model.User;

public class UserIdentifierImpl implements UserIdentifier {

    Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public String getSearchCriteria(User user) {

        logger.info("[** LDAP UPDATE **] getSearchCriteria for ldap user: " + user.getFullname() + " (sn="
                + user.getDescription() + " => Might be updated)\n\n");

        return "(sn=" + user.getDescription() + ")";
    }
}
