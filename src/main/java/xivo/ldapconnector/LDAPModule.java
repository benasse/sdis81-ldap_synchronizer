package xivo.ldapconnector;

import xivo.ldap.AbstractLDAPModule;
import xivo.ldap.ldapconnection.UserIdentifier;

public class LDAPModule extends AbstractLDAPModule {

    public LDAPModule(boolean dryRun) {
        super(dryRun);
    }

    @Override
    protected void specialBinds() {
        bind(xivo.ldap.ldapconnection.UserModifier.class).to(UserModifierImpl.class);
        bind(xivo.ldap.Initializer.class).to(InitializerImpl.class);
        bind(xivo.ldap.AbstractDeltaGenerator.class).to(DeltaGenerator.class);
        bind(xivo.ldap.AbstractLdapSynchronizer.class).to(LDAPSynchronizer.class);
        bind(UserIdentifier.class).to(UserIdentifierImpl.class);
    }

}