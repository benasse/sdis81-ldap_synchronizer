package xivo.ldapconnector;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import xivo.restapi.model.IncomingCall;
import xivo.restapi.model.Line;
import xivo.restapi.model.User;
import xivo.restapi.model.Voicemail;
import java.util.logging.Logger;


public class TestDeltaGenerator extends TestCase {

	Logger logger = Logger.getLogger(getClass().getName());
	
    private DeltaGenerator generator;

    @Before
    public void setUp() throws Exception {
        generator = new DeltaGenerator();
    }

    public String formatWithSpace(String Number) {
		String first;
		String second;
		String third;
		String fourth;
		String fifth;
		Number=Number.replaceAll("\\s", "");
		first = Number.substring(0,2);
		second = Number.substring(2,4);
		third = Number.substring(4,6);
		fourth = Number.substring(6,8);
		fifth = Number.substring(8,10);
		
		Number=first+" "+second+" "+third+" "+fourth+" "+fifth;
		
		return Number;
    }
    @Test
    public void testXivoNeedsUpdateTrue() {
    	// test modification du nom
        User xivoUser = new User(), ldapUser = new User();
        xivoUser.setFirstname("Pierre");
        ldapUser.setFirstname("Pierre");
        xivoUser.setLastname("Dupond");
        ldapUser.setLastname("Dupont");
        xivoUser.setLine(new Line("1234"));
        xivoUser.setPassword("1234");
        logger.info("test numero 0632890742 "+ formatWithSpace("0632890742"));
 
        assertTrue(generator.xivoNeedsUpdate(xivoUser, ldapUser));
        
        // test modification du prenom
        xivoUser.setFirstname("Pierre");
        ldapUser.setFirstname("Pierr");
        xivoUser.setLastname("Dupond");
        ldapUser.setLastname("Dupond");
        

        assertTrue(generator.xivoNeedsUpdate(xivoUser, ldapUser));

        // test modification de l'email
        User xivoUserSerge = new User(), ldapUserSerge = new User();
        xivoUserSerge.setFirstname("Serge");
        xivoUserSerge.setLastname("MIMI");
        xivoUserSerge.setEmail("serge.mimi@orange.com");
        ldapUserSerge.setFirstname("Serge");
        ldapUserSerge.setLastname("MIMI");
        ldapUserSerge.setEmail("serge.mimi@free.fr");

        assertTrue(generator.xivoNeedsUpdate(xivoUserSerge, ldapUserSerge));

        // test creation email
        User xivoUserAccusateur = new User(), ldapUserAccusateur = new User();
        xivoUserAccusateur.setFirstname("Ronan");
        xivoUserAccusateur.setLastname("ACCUSATEUR");
        xivoUserAccusateur.setEmail("Ronan.ACCUSATEUR@kree.com");
        ldapUserAccusateur.setFirstname("Ronan");
        ldapUserAccusateur.setLastname("ACCUSATEUR");

        assertTrue(generator.xivoNeedsUpdate(xivoUserAccusateur, ldapUserAccusateur));

        // test modification login xivo client
        User xivoUserGuardian = new User(), ldapUserGuardian = new User();
        xivoUserGuardian.setUsername("drax");
        ldapUserGuardian.setUsername("gamora");
       
        assertTrue(generator.xivoNeedsUpdate(xivoUserGuardian, ldapUserGuardian));
        
        
        
        // test modification du passwd xivo client
        
        xivoUserGuardian.setLine(new Line("1234"));
        xivoUserGuardian.setPassword("1235");
        ldapUserGuardian.setPassword("gamora");

        assertTrue(generator.xivoNeedsUpdate(xivoUserGuardian, ldapUserGuardian));
        
        // test de modification du userfield.
        User xivoUserfield = new User(), ldapUserfield = new User();
        xivoUserfield.setUserfield("gamora");
        ldapUserfield.setUserfield("test");
        
        assertTrue(generator.xivoNeedsUpdate(xivoUserfield, ldapUserfield));
        
     // Tests de voicemail
     /*   User xivoUserWithMevo = new User(), ldapUserWithMevo = new User();
        Voicemail xivoUserVoicemail = new Voicemail();
        xivoUserVoicemail.setEmail("gesnaud@avencall.com");
        xivoUserVoicemail.setName("Gesnaud");
        xivoUserWithMevo.setVoicemail(xivoUserVoicemail);
        xivoUserWithMevo.setFirstname("Messagerie");
        xivoUserWithMevo.setLastname("VOCALE");

        ldapUserWithMevo.setFirstname("Messagerie");
        ldapUserWithMevo.setLastname("VOCALE");
        Voicemail ldapUservoicemail = new Voicemail();
        ldapUservoicemail.setEmail("gesnaud@cisco.com");
        ldapUservoicemail.setName("blabla");
        ldapUserWithMevo.setVoicemail(ldapUservoicemail);

        assertTrue(generator.xivoNeedsUpdate(xivoUserWithMevo, ldapUserWithMevo));
        */

    }

    @Test
    public void testXivoNeedsUpdateFalse() {

        User xivoUserPeter = new User(), ldapUserPeter = new User();
        Voicemail xivoUserVoicemail = new Voicemail(), ldapUserVoicemail = new Voicemail();
        xivoUserPeter.setFirstname("Peter");
        xivoUserPeter.setLastname("Quin");
        ldapUserPeter.setFirstname("Peter");
        ldapUserPeter.setLastname("Quin");
        xivoUserPeter.setEmail("peter@guardian.com");
        ldapUserPeter.setEmail("peter@guardian.com");
        xivoUserPeter.setUsername("peter");
        ldapUserPeter.setUsername("peter");
        xivoUserPeter.setLine(new Line("1234"));
        xivoUserPeter.setPassword("B!ds5:J6gfy8M$Ugy6JB");
        xivoUserVoicemail.setEmail("gesnaud@avencall.com");
        xivoUserVoicemail.setName("Gesnaud");
        ldapUserVoicemail.setEmail("gesnaud@avencall.com");
        ldapUserVoicemail.setName("Gesnaud");
        xivoUserPeter.setVoicemail(xivoUserVoicemail);
        ldapUserPeter.setVoicemail(ldapUserVoicemail);
        ldapUserPeter.setMobilephonenumber("06 32 89 07 42");
        xivoUserPeter.setMobilephonenumber("0632890742");
        
        assertTrue(generator.xivoNeedsUpdate(xivoUserPeter, ldapUserPeter));
    }

    @Test
    public void testLdapNeedsUpdateTrue() {
    	// test MAJ ligne interne
        User xivoUser = new User(), ldapUser = new User();
        
        xivoUser.setLine(new Line("1234"));
        ldapUser.setLine(new Line("4567"));
        xivoUser.getLine().setprovisioningCode("987654");
        assertTrue(generator.ldapNeedsUpdate(xivoUser, ldapUser));

        // test création ligne interne
        xivoUser.setLine(new Line("1234"));
        ldapUser.setLine(null);
        assertTrue(generator.ldapNeedsUpdate(xivoUser, ldapUser));
/*
        // test modification ligne SDA
        xivoUser.setLine(null);
        ldapUser.setLine(null);
        xivoUser.setIncomingCall(new IncomingCall("102053015"));
        ldapUser.setIncomingCall(new IncomingCall("01 01 01 30 17"));
        assertTrue(generator.ldapNeedsUpdate(xivoUser, ldapUser));

        // test création ligne SDA
        xivoUser.setIncomingCall(new IncomingCall("0101023015"));
        ldapUser.setIncomingCall(null);

        /* // test création numéro Mobile
        xivoUser.setIncomingCall(null);
        xivoUser.setMobilephonenumber("0605040302");
        ldapUser.setMobilephonenumber(null);
        
        //assertTrue(generator.ldapNeedsUpdate(xivoUser, ldapUser));
        
        // test de maj numéro mobile
        ldapUser.setMobilephonenumber("0650841233");
        assertTrue(generator.ldapNeedsUpdate(xivoUser, ldapUser)); */

		// test de maj du prov code
		ldapUser.setLine(new Line("1234"));
		xivoUser.getLine().setprovisioningCode("123456");
		ldapUser.getLine().setprovisioningCode("123457");
		assertTrue(generator.ldapNeedsUpdate(xivoUser, ldapUser));
	    
		

    }

    @Test
    public void testLdapNeedsUpdateFalse() {
        User xivoUserFalse = new User(), ldapUserFalse = new User();
        // pas de maj de la ligne
        xivoUserFalse.setLine(new Line("1234"));
        ldapUserFalse.setLine(new Line("1234"));
        xivoUserFalse.getLine().setprovisioningCode("123456");
        ldapUserFalse.getLine().setprovisioningCode("123456");
        assertFalse(generator.ldapNeedsUpdate(xivoUserFalse, ldapUserFalse));
        /*
        // pas de maj de la SDA
        xivoUserFalse.setIncomingCall(new IncomingCall("102033016"));
        ldapUserFalse.setIncomingCall(new IncomingCall("01 02 03 30 16"));
        assertFalse(generator.ldapNeedsUpdate(xivoUserFalse, ldapUserFalse));
        
        // pas de maj du mobile
        xivoUserFalse.setMobilephonenumber("0605040302");
        ldapUserFalse.setMobilephonenumber("06 05 04 03 02");
        //assertFalse(generator.ldapNeedsUpdate(xivoUserFalse, ldapUserFalse));
         */
        // pas de maj du prov code
       
    }
}
