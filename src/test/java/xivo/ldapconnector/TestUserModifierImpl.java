package xivo.ldapconnector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;
import xivo.ldap.ByteDecoder;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import xivo.restapi.model.User;
import xivo.restapi.model.Line;
import xivo.restapi.model.Voicemail;

/* public class TestUserModifierImpl {

    private UserModifierImpl modifier;
    private User user,user_otc;
    private Line line,line_otc;
    private Attributes attrs;
    private SearchResult sr;
    private Attribute telephoneNumber;
    private NamingEnumeration numbersEnum;
    private ByteDecoder decoder;

    @Before
    public void setUp() {
        modifier = new UserModifierImpl();

        user = new User();
        line = new Line();
        line_otc = new Line();
        user_otc = new User();
        
        line.setContext("default");
        line.setNumber("3000");
        user.setFirstname("Pierre");
        user.setLastname("Dupond");
        user.setLine(line);
        
        line_otc.setContext("default");
        line_otc.setNumber("5000");
        user_otc.setFirstname("JB");
        user_otc.setLastname("Marin");
        user_otc.setLine(line_otc);       
        
        

        sr = EasyMock.createMock(SearchResult.class);
        attrs = EasyMock.createMock(Attributes.class);
        telephoneNumber = EasyMock.createMock(Attribute.class);
        numbersEnum = EasyMock.createMock(NamingEnumeration.class);
    }

    @Test
    public void testModifyUser_default() throws NamingException {
    	SearchResult sr = EasyMock.createMock(SearchResult.class);
        EasyMock.expect(sr.getAttributes()).andReturn(attrs);
        EasyMock.expect(attrs.get("telephoneNumber")).andReturn(telephoneNumber);
        EasyMock.expect(telephoneNumber.getAll()).andReturn(numbersEnum);
        EasyMock.expect(numbersEnum.hasMoreElements()).andReturn(true);
        EasyMock.expect(numbersEnum.next()).andReturn("3000");
        EasyMock.expect(numbersEnum.hasMoreElements()).andReturn(false);
        EasyMock.replay(sr, attrs, telephoneNumber, numbersEnum);

        User res = modifier.modifyXivoUser(user, sr);

        //EasyMock.verify(sr, attrs, telephoneNumber, numbersEnum);
        assertEquals("3000", res.getLine().getNumber());
        assertEquals("default", res.getLine().getContext());
    }

    @Test
    public void testModifyUser_otc() throws NamingException {
    	SearchResult sr = EasyMock.createMock(SearchResult.class);
         EasyMock.expect(sr.getAttributes()).andReturn(attrs);
        EasyMock.expect(attrs.get("telephoneNumber")).andReturn(telephoneNumber);
        EasyMock.expect(telephoneNumber.getAll()).andReturn(numbersEnum);
        EasyMock.expect(numbersEnum.hasMoreElements()).andReturn(true);
        EasyMock.expect(numbersEnum.next()).andReturn("5000");
        EasyMock.expect(numbersEnum.hasMoreElements()).andReturn(false);
        EasyMock.replay(sr, attrs, telephoneNumber, numbersEnum); 

        User res_otc = modifier.modifyXivoUser(user_otc, sr);

        //EasyMock.verify(sr, attrs, telephoneNumber, numbersEnum);
        assertEquals("5000", res_otc.getLine().getNumber());
        assertEquals("otc", res_otc.getLine().getContext());
    }

} */
