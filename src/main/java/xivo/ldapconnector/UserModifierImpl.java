package xivo.ldapconnector;


import xivo.ldap.xivoconnection.*;

import java.util.LinkedList;
import java.util.logging.Logger;

import javax.naming.NamingException;

import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;

import com.google.inject.Inject;

import xivo.ldap.configuration.ConfigLoader;
import xivo.ldap.ldapconnection.UserModifier;
import xivo.restapi.model.User;
import java.sql.SQLException;
import xivo.restapi.model.IncomingCall;
import xivo.restapi.model.Line;
import xivo.restapi.model.Context;
import xivo.restapi.model.ContextInterval;

import xivo.ldap.ByteDecoder;


public class UserModifierImpl implements UserModifier {
	

    Logger logger = Logger.getLogger(getClass().getName());


    public User modifyXivoUser(User user, SearchResult searchResult) throws NamingException {
   
     
    	//modification du numéro SDA sur 9 digits
    	
    	if (user.getIncomingCall() != null) {
    		IncomingCall sda = user.getIncomingCall();
    		logger.info(" SDA from LDAP : "+ user.getIncomingCall().getSda());
    		sda.setSda("0" + user.getIncomingCall().getSda().replaceAll("\\W","").substring(user.getIncomingCall().getSda().replaceAll("\\W","").length() -9,user.getIncomingCall().getSda().replaceAll("\\W","").length()));
    		logger.info("  CHANGE SDA FROM LDAP TO XIVO :  "+ "0" +sda.getSda());
    	}
    	
    	//modification du numéro de mobile
    	if (user.getMobilephonenumber() != null) {
    		user.setMobilephonenumber("0"+user.getMobilephonenumber().replaceAll("\\W","").substring(user.getMobilephonenumber().replaceAll("\\W","").length() -9,user.getMobilephonenumber().replaceAll("\\W","").length()));
    		logger.info("  CHANGE MOBILE FROM LDAP TO XIVO ");
    	}
    	
    	// modification du callerID selon le format XIVO
    	String callerid = "\"" + user.getFirstname() + " " + user.getLastname() + "\"";
    	user.setCallerid(callerid);
        user.setPassword("Bds5J6gfy8MUgy6JB");
        return user;
    }
    
    public String formatWithSpace(String Number) {
    	// ici les numéros sont sous la forme 5XXXXXXXX
    	// il faut donc rajouter un 0 au 5 
		String first;
		String second;
		String third;
		String fourth;
		String fifth;
		
		first = "0"+Number.substring(0,1);
		second = Number.substring(1,3);
		third = Number.substring(3,5);
		fourth = Number.substring(5,7);
		fifth = Number.substring(7,9);
		
		Number=first+" "+second+" "+third+" "+fourth+" "+fifth;
		
		return Number;
    }
    public User modifyLdapUser(User user) {
    	
    	// ecriture de l'utilisateur avec numero de modible du format 06 XX XX XX XX
    	// écritue de l'utilisateur avec numéro de SDA au format 01 XX XX XX XX
    	
   
    
    	
    	/*if (user.getIncomingCall() != null) {
	    	 if (user.getIncomingCall().getSda().length() == 9) {
	             logger.info("[** LDAP User MODIFIER **] LDAP user " + user.getFullname() + ": update telephoneNumber from "
	                     + "0"+user.getIncomingCall().getSda() +" to : "+ formatWithSpace(user.getIncomingCall().getSda()));
	           
	             user.getIncomingCall().setSda(formatWithSpace(user.getIncomingCall().getSda()));
	         }
    	}
    	
    	*/
    	
        return user;
    }

}
